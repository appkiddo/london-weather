package in.codeseed.weatherwatch.home.model;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public interface IHomeInteractor {

    void loadWeather(OnWeatherLoadingFinishedCallback callback);
}
