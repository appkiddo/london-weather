package in.codeseed.weatherwatch.home.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public class WeatherDate {

    @SerializedName("dt")
    private long date;

    @SerializedName("temp")
    private Temp temp;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Temp getTemp() {
        return temp;
    }

    public void setTemp(Temp temp) {
        this.temp = temp;
    }
}
