package in.codeseed.weatherwatch.home.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import in.codeseed.weatherwatch.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_frame, HomeFragment.newInstance(), HomeFragment.class.getSimpleName())
                    .commit();
        }
    }
}
