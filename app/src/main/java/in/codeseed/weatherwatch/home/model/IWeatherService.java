package in.codeseed.weatherwatch.home.model;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public interface IWeatherService {

    @GET("/data/2.5/forecast/daily")
    Call<WeatherForecast> loadWeatherForecast(@QueryMap Map<String, String> queryParam);
}
