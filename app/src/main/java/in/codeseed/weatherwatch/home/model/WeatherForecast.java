package in.codeseed.weatherwatch.home.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public class WeatherForecast {

    @SerializedName("list")
    private List<WeatherDate> weatherDateList = new ArrayList<>();

    public List<WeatherDate> getWeatherDateList() {

        return weatherDateList;
    }

    public void setWeatherDateList(List<WeatherDate> weatherDateList) {

        this.weatherDateList = weatherDateList;
    }
}
