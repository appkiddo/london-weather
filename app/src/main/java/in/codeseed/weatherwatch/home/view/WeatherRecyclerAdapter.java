package in.codeseed.weatherwatch.home.view;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import in.codeseed.weatherwatch.R;
import in.codeseed.weatherwatch.home.model.WeatherDate;
import in.codeseed.weatherwatch.util.DateUtil;
import in.codeseed.weatherwatch.util.ConversionUtil;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public class WeatherRecyclerAdapter extends RecyclerView.Adapter<WeatherRecyclerAdapter.ViewHolder> {

    private static final String TAG = WeatherRecyclerAdapter.class.getSimpleName();
    private List<WeatherDate> weatherDateList;

    public WeatherRecyclerAdapter(List<WeatherDate> weatherDateList) {

        this.weatherDateList = weatherDateList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weather_recycler, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String dateText = DateUtil.getFormattedDateText(weatherDateList.get(position).getDate());
        holder.date.setText(dateText);

        String maxTemp = String.format(
                Locale.getDefault(),
                " %s°",
                ConversionUtil.getDecimalValue(weatherDateList.get(position).getTemp().getMax())
        );
        String minTemp = String.format(
                Locale.getDefault(),
                " %s°",
                ConversionUtil.getDecimalValue(weatherDateList.get(position).getTemp().getMin())
        );
        holder.minTemp.setText(minTemp);
        holder.maxTemp.setText(maxTemp);
    }

    @Override
    public int getItemCount() {

        Log.d(TAG, "WeatherDateList Size - " + weatherDateList.size());
        return weatherDateList == null ? 0 : weatherDateList.size();
    }

    public void update(List<WeatherDate> weatherDateList) {

        this.weatherDateList = weatherDateList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.date) TextView date;
        @Bind(R.id.min_temp) TextView minTemp;
        @Bind(R.id.max_temp) TextView maxTemp;

        public ViewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            String dateText = DateUtil.getFormattedDateText(weatherDateList.get(getAdapterPosition()).getDate());
            Toast.makeText(v.getContext(), String.format(Locale.getDefault(), "Weather Report on %s", dateText), Toast.LENGTH_SHORT).show();
        }
    }
}
