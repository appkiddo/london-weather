package in.codeseed.weatherwatch.home.presenter;

import java.util.List;

import in.codeseed.weatherwatch.home.model.IHomeInteractor;
import in.codeseed.weatherwatch.home.model.OnWeatherLoadingFinishedCallback;
import in.codeseed.weatherwatch.home.model.WeatherDate;
import in.codeseed.weatherwatch.home.view.IHomeView;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public class HomePresenter implements IHomePresenter, OnWeatherLoadingFinishedCallback {

    private IHomeView homeView;
    private IHomeInteractor homeInteractor;

    public HomePresenter(IHomeView homeView, IHomeInteractor homeInteractor) {

        this.homeView = homeView;
        this.homeInteractor = homeInteractor;
    }

    @Override
    public void loadWeather() {

        homeView.showProgress();
        homeInteractor.loadWeather(this);
    }

    @Override
    public void onError() {

        homeView.showMessage();
    }

    @Override
    public void onLoadingFinished(List<WeatherDate> weatherDateList) {

        homeView.updateWeatherForecast(weatherDateList);
    }

    @Override
    public void onDestroy() {

        this.homeView = null;
    }
}
