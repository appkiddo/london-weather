package in.codeseed.weatherwatch.home.presenter;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public interface IHomePresenter {

    void loadWeather();
    void onDestroy();
}
