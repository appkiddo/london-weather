package in.codeseed.weatherwatch.home.model;

import java.util.List;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public interface OnWeatherLoadingFinishedCallback {

    void onError();
    void onLoadingFinished(List<WeatherDate> weatherDateList);
}
