package in.codeseed.weatherwatch.home.model;

import android.util.Log;

import java.util.HashMap;

import in.codeseed.weatherwatch.BuildConfig;
import in.codeseed.weatherwatch.util.NetworkConstants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public class HomeInteractor implements IHomeInteractor {

    private static final String TAG = HomeInteractor.class.getSimpleName();
    private IWeatherService weatherService;

    public HomeInteractor() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NetworkConstants.WEATHER_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        weatherService = retrofit.create(IWeatherService.class);
    }

    @Override
    public void loadWeather(final OnWeatherLoadingFinishedCallback callback) {

        HashMap<String, String> weatherParam = getQueryParams();

        final Call<WeatherForecast> weatherForecastCall = weatherService.loadWeatherForecast(weatherParam);
        weatherForecastCall.enqueue(new Callback<WeatherForecast>() {

            @Override
            public void onResponse(Call<WeatherForecast> call, Response<WeatherForecast> response) {

                Log.d(TAG, "onResponse");
                for (WeatherDate weatherDate : response.body().getWeatherDateList()) {

                    Log.d(TAG, weatherDate.getDate() + "");
                }

                callback.onLoadingFinished(response.body().getWeatherDateList());
            }

            @Override
            public void onFailure(Call<WeatherForecast> call, Throwable t) {

                Log.d(TAG, "onFailure");

                callback.onError();
            }
        });
    }

    private HashMap<String, String> getQueryParams() {

        HashMap<String, String> weatherParm = new HashMap<>();
        weatherParm.put("q", "London");
        weatherParm.put("units", "metric");
        weatherParm.put("cnt", "7");
        weatherParm.put("appid", BuildConfig.WEATHER_API_KEY);
        return weatherParm;
    }
}
