package in.codeseed.weatherwatch.home.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import in.codeseed.weatherwatch.R;
import in.codeseed.weatherwatch.home.model.HomeInteractor;
import in.codeseed.weatherwatch.home.model.WeatherDate;
import in.codeseed.weatherwatch.home.presenter.HomePresenter;
import in.codeseed.weatherwatch.home.presenter.IHomePresenter;

/**
 * Created by Balachandar Kolathur Mani on 30/03/2016.
 */
public class HomeFragment extends Fragment implements IHomeView {

    private static final String TAG = HomeFragment.class.getSimpleName();

    @Bind(R.id.weather_recycler) RecyclerView weatherRecycler;
    @Bind(R.id.progressbar) ProgressBar progressBar;
    @Bind(R.id.message) TextView message;

    private IHomePresenter homePresenter;
    private WeatherRecyclerAdapter weatherRecyclerAdapter;

    public static HomeFragment newInstance() {

        return new HomeFragment();
    }

    public HomeFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        homePresenter = new HomePresenter(this, new HomeInteractor());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.home_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_refresh:
                homePresenter.loadWeather();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        setToolbar();
        setupWeatherRecycler();

        homePresenter.loadWeather();
    }

    private void setToolbar() {


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        homePresenter.onDestroy();
    }

    @Override
    public void showProgress() {

        Log.d(TAG, "showProgress");

        weatherRecycler.setVisibility(View.INVISIBLE);
        message.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateWeatherForecast(List<WeatherDate> weatherDateList) {

        Log.d(TAG, "updateWeatherForecast");
        weatherRecyclerAdapter.update(weatherDateList);
        weatherRecyclerAdapter.notifyDataSetChanged();

        message.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        weatherRecycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage() {

        Log.d(TAG, "showMessage");

        weatherRecycler.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        message.setVisibility(View.VISIBLE);
    }

    private void setupWeatherRecycler() {

        weatherRecyclerAdapter = new WeatherRecyclerAdapter(new ArrayList<WeatherDate>());
        weatherRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        weatherRecycler.setAdapter(weatherRecyclerAdapter);
    }
}
