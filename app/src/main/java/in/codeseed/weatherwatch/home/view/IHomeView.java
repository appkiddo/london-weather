package in.codeseed.weatherwatch.home.view;

import java.util.List;

import in.codeseed.weatherwatch.home.model.WeatherDate;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public interface IHomeView {

    void showProgress();
    void updateWeatherForecast(List<WeatherDate> weatherDateList);
    void showMessage();
}
