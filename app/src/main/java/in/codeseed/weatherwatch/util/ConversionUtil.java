package in.codeseed.weatherwatch.util;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public class ConversionUtil {

    public static String getDecimalValue(Double temp) {

        return String.valueOf(temp.intValue());
    }
}
