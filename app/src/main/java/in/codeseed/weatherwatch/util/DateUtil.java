package in.codeseed.weatherwatch.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public class DateUtil {


    public static String getFormattedDateText(long epochTimeInSeconds) {

        Date date = new Date(epochTimeInSeconds * 1000L);
        return new SimpleDateFormat("EEE, dd MMM", Locale.getDefault()).format(date);
    }


}
