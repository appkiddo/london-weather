package in.codeseed.weatherwatch.home.presenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import in.codeseed.weatherwatch.home.model.IHomeInteractor;
import in.codeseed.weatherwatch.home.model.OnWeatherLoadingFinishedCallback;
import in.codeseed.weatherwatch.home.model.WeatherDate;
import in.codeseed.weatherwatch.home.view.IHomeView;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest {

    @Mock
    IHomeView mockHomeView;
    @Mock
    IHomeInteractor mockHomeInteractor;
    @Mock
    List<WeatherDate> mockWeatherDateList;

    private HomePresenter homePresenter;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        homePresenter = new HomePresenter(mockHomeView, mockHomeInteractor);
    }

    @After
    public void tearDown() throws Exception {

        homePresenter = null;
    }

    @Test
    public void testLoadWeather() throws Exception {

        homePresenter.loadWeather();

        verify(mockHomeView).showProgress();
        verify(mockHomeInteractor).loadWeather(isA(OnWeatherLoadingFinishedCallback.class));
    }

    @Test
    public void testOnError() throws Exception {

        homePresenter.onError();

        verify(mockHomeView).showMessage();
    }

    @Test
    public void testOnLoadingFinished() throws Exception {

        homePresenter.onLoadingFinished(mockWeatherDateList);

        verify(mockHomeView).updateWeatherForecast(mockWeatherDateList);
    }
}