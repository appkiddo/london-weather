package in.codeseed.weatherwatch.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public class ConversionUtilTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getDecimalValue_ReturnsDecimalPart() throws Exception {

        //Arrange
        String expectedDecimalString = "33";
        Double input = 33.333;

        //Act
        String actualDecimalString = ConversionUtil.getDecimalValue(input);

        //Arrange
        assertEquals(expectedDecimalString, actualDecimalString);
    }

    @Test
    public void getDecimalValue_ReturnsDecimalPartWhenThereIsNoFraction() throws Exception {

        //Arrange
        String expectedDecimalString = "33";
        Double input = 33d;

        //Act
        String actualDecimalString = ConversionUtil.getDecimalValue(input);

        //Arrange
        assertEquals(expectedDecimalString, actualDecimalString);
    }

    @Test
    public void getDecimalValue_ReturnsZeroWhenThereIsNoDecimal() throws Exception {

        //Arrange
        String expectedDecimalString = "0";
        Double input = .333;

        //Act
        String actualDecimalString = ConversionUtil.getDecimalValue(input);

        //Arrange
        assertEquals(expectedDecimalString, actualDecimalString);
    }
}