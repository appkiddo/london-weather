package in.codeseed.weatherwatch.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Balachandar Kolathur Mani on 31/03/2016.
 */
public class DateUtilTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getFormattedDateText_ReturnsCorrectFormattedDate() throws Exception {

        //Arrange
        String expectedDateText = "Thu, 31 Mar";
        long epochTime = 1459420973;

        //Act
        String actualDateText = DateUtil.getFormattedDateText(epochTime);

        //Assert
        assertEquals(expectedDateText, actualDateText);
    }
}