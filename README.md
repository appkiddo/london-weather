# README #

A simple android application to show the weather forecast of London for seven days. It is built based on MVP architecture.

It relies on http://openweathermap.org/api for weather data.

![Screen Shot 2016-03-31 at 13.09.38.png](https://bitbucket.org/repo/BEKE6n/images/819865199-Screen%20Shot%202016-03-31%20at%2013.09.38.png)